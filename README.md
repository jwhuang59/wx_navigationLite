## 导航lite
地图导航、路线导航、公交地铁换乘方案


#### 效果截图


<img src="https://images.gitee.com/uploads/images/2019/1212/163448_1cb55db5_5543907.jpeg" width="280"> 
 <img src="https://raw.githubusercontent.com/jwhuang59/wx_navigationLite/master/screenshot/2.jpg" width="280">
  <img src="https://images.gitee.com/uploads/images/2019/1212/163448_af262fa0_5543907.jpeg" width="280">
   <img src="https://images.gitee.com/uploads/images/2019/1212/163450_fbb224e1_5543907.jpeg" width="280"> 
   <img src="https://images.gitee.com/uploads/images/2019/1212/163449_b412800a_5543907.jpeg" width="280">
   <img src="https://images.gitee.com/uploads/images/2019/1212/163451_5047206e_5543907.jpeg" width="280">
   <img src="https://images.gitee.com/uploads/images/2019/1212/163453_761bb179_5543907.jpeg" width="280">


#### 安装与运行

``` bash
安装微信开发者工具。 把项目下载到本地。 在微信开发者工具中打开该项目即可预览。
```


#### 在线访问

<img src="https://gitee.com/jwhuang59/wx_navigationLite/raw/master/screenshot/visit.png" width="280">

#### 鼠标右上角star，谢谢！

